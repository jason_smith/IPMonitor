﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IPMonitor
{
    public partial class IPMonitor : ServiceBase
    {
        #region Fields

        const string IPMon = "IPMonitor";
        const string IPMonLog = "IPMonitorLog";
        const int Port = 587;
        const string Pwd = ""; // do not store password in code hosted on a public server 
        const string SmtpServer = "smtp-mail.outlook.com";
        const string Subject = "IP Address Changed";
        const string ToEmailAddr = "smithje@zoho.com";
        const string User = "jason.e.smith@live.com";

        readonly EventLog _eventLog;

        #endregion

        #region Constructors

        public IPMonitor()
        {
            InitializeComponent();

            _eventLog = new EventLog();
            if (!EventLog.SourceExists(IPMon))
                EventLog.CreateEventSource(IPMon, IPMonLog);

            _eventLog.Source = IPMon;
        }

        #endregion

        #region Methods


        private string GetNetworkInfoMsg()
        {
            IEnumerable<NetworkInterface> adapters = NetworkInterface.GetAllNetworkInterfaces()
                .Where(i => i.OperationalStatus == OperationalStatus.Up);

            var sb = new StringBuilder();
            foreach (NetworkInterface adapter in adapters)
            {
                UnicastIPAddressInformation info = adapter.GetIPProperties().UnicastAddresses
                    .FirstOrDefault(a => a.Address.AddressFamily == AddressFamily.InterNetwork);

                if (info != null)
                    sb.AppendLine(string.Format("{0}: {1}", adapter.Name, info.Address.ToString()));
            }

            return sb.ToString();
        }


        private void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
        {
            string msg = this.GetNetworkInfoMsg();
            this.SendEmail(msg);
        }


        protected override void OnStart(string[] args)
        {
            _eventLog.WriteEntry("IPMonitor started.", EventLogEntryType.Information);

            Task.Run(() =>
            {
                string info = this.GetNetworkInfoMsg();
                this.SendEmail(info);
            });

            NetworkChange.NetworkAddressChanged += this.NetworkChange_NetworkAddressChanged;
        }


        protected override void OnStop()
        {
            _eventLog.WriteEntry("IPMonitor stopped.", EventLogEntryType.Information);
            NetworkChange.NetworkAddressChanged -= this.NetworkChange_NetworkAddressChanged;
        }


        private void SendEmail(string msg)
        {
            SmtpClient client = null;
            MailMessage mailMsg = null;

            try
            {
                _eventLog.WriteEntry("Sending email:\r\n" + msg, EventLogEntryType.Information);

                mailMsg = new MailMessage(User, ToEmailAddr, Subject, msg);

                client = new SmtpClient(SmtpServer, Port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = true;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(User, Pwd);
                client.Send(mailMsg);

                _eventLog.WriteEntry("Email sent successfully.");
            }
            catch (Exception ex)
            {
                _eventLog.WriteEntry(ex.Message + "\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            finally
            {
                if (mailMsg != null)
                    mailMsg.Dispose();

                if (client != null)
                    client.Dispose();
            }
        }

        #endregion
    }
}
